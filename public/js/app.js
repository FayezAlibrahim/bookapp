function showExpertPage(id) {
  var offset=document.getElementById("timeZoneList"); //get selected time zone from dropdown
  $.ajax({  //get user geolocation info (ip, timezone ,city ...)
    beforeSend: function(){
      $('#loading').show();
  },
  complete: function(){
      $('#loading').hide();
  },
    url: 'https://api.ipgeolocation.io/ipgeo?apiKey=57aa4ceeb6b749e38f202d82f7835946',
    type: 'GET',
    dataType: 'JSON',
    success: function (data) {
      var os=offset.value=="all"?data.time_zone.offset:offset.value;
      var city=offset.value=="all"?data.city:offset.options[offset.selectedIndex].label;
      location.href = '/experts/'+id+'?city='+encodeURIComponent(city)+'&offset='+encodeURIComponent(os);
    }
});
}


function generateTimeSlots(nS,rT,sT,eT){
  var x = {
    nextSlot: nS,
    // reservedTime: [
    //     ['11:00', '14:00'], ['16:00', '18:00'],
    // ],
    reservedTime: rT,

    startTime: sT,//'8:00',
    endTime: eT,//'20:00'
  };
  var slotTime = moment(x.startTime, "HH:mm");
  var endTime = moment(x.endTime, "HH:mm");

  function isInBreak(slotTime, breakTimes) {
    return breakTimes.some((br) => {
        return  slotTime >= moment(br[0].split(',')[0], "HH:mm") && slotTime < moment(br[0].split(',')[1], "HH:mm");
  });
  }

  let times = [];
  while (slotTime < endTime)
  {
  if (!isInBreak(slotTime, x.reservedTime)) {
      var timeToPush=[];
      timeToPush[0]=slotTime.format("HH:mm");
      timeToPush[1]=slotTime.add(x.nextSlot, 'minutes').format("HH:mm");
      if(slotTime <= endTime)
              times.push(timeToPush);
      slotTime = slotTime.subtract(x.nextSlot, 'minutes');
    }
  slotTime = slotTime.add(15, 'minutes');
  }
  return times;
}

$('input[type=radio][name=timeslot]').change(function() {
  changeWorkingHours(this.value);
});


$(function () {
  $('#date').datepicker({
            dateFormat: 'yy-mm-dd',
            autoclose: true,
            minDate:'d',
            date:'d'
        });
         });

function parseTime(t) {
    var d = new Date();
    var time = t.match(/(\d+)(?::(\d\d))?\s*(p?)/);
    d.setHours(parseInt(time[1]) + (time[3] ? 12 : 0));
    d.setMinutes(parseInt(time[2]) || 0);
    return d.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
}
