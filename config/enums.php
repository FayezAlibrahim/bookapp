<?php

return [
    'timezones' => [
        '-11' => 'Midway Island',
        '-10' => 'Hawaii!',
        '-9' => 'Alaska',
        '-8' => 'Pacific Time (US Canada)',
        '-7' => 'Mountain Time (US  Canada)',
        '-6' => 'Central Time (US  Canada)',
        '-5' => 'Eastern Time (US  Canada)',
        '-4' => 'Atlantic Time (Canada)',
        '-3' => 'Brasilia',
        '-2' => 'Mid-Atlantic',
        '-1' => 'Azores',
        '0' => 'London',
        '1' => 'Berlin',
        '2' => 'Cairo',
        '2' => 'Syria',
        '3' => 'Kuwait',
        '4' => 'Abu Dhabi',
        '5' => 'Islamabad',
        '6' => 'Dhaka',
        '7' => 'Jakarta',
        '8' => 'Beijing',
        '9' => 'Tokyo',
        '10' => 'Brisbane',
        '11' => 'Magadan',
        '12' => 'Fiji',

    ],
];