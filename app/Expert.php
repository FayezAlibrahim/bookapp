<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Reservation;

class Expert extends Model
{
    protected $dates = ['start_time','close_time'];
protected $casts=[
    'time'=> 'time:hh:mm'
];
    public function getReservedTimes(){
        $stringToReturn=[];
$rt=Reservation::where('expert_id',$this->id)->where('status','open')->pluck('time');
foreach($rt as $reserved_time){
            $reserved_time= explode(',', $reserved_time);
// array_push($dataToReturn, ''. $reserved_time[0] . '","' . $reserved_time[1].'');
// $= $.'['.$reserved_time[0].' '."," .''. $reserved_time[1] .']'."-";
array_push($stringToReturn,[$reserved_time[0].','. $reserved_time[1]]);
}
return $stringToReturn;
    }
}
