<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = ['user_id','expert_id', 'time','duration', 'date'];
     protected $tabel= "reservations";
}
