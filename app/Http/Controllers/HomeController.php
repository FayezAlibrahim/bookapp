<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expert;
use Config;

class HomeController extends Controller
{

    public function index()
    {
        $timezones=Config::get('enums.timezones');
        $experts=Expert::get();
        return view('welcome',compact('timezones','experts'));
    }
}
