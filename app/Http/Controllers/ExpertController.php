<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expert;

use Config;

class ExpertController extends Controller
{
    public function expertByID($exp_id){
        $expert=Expert::findOrFail($exp_id);
        return view('experts.show',compact('expert'));
    }
}
