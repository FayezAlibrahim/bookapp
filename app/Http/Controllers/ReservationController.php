<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Reservation;
class ReservationController extends Controller
{
    private function RegisterValidations()
    {
        return [
            'user_id'   => 'required|exists:users,id',
            'expert_id'   => 'required|string|exists:experts,id',
            'date'     => 'required|date|after_or_equal:' . date('Y-m-d'),
            'duration'       => 'required|in:15,30,45,60',
            'time' => 'required|string'
        ];
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), $this->RegisterValidations());
        if ($validator->fails()) {
           //log here
          //detailed errors
            return response()->json(['status' => 'fail', 'message' =>"Validation Error" ]);
        }
        DB::beginTransaction();
        $validated_data = $validator->validated();
        $reservation = Reservation::create($validated_data);
        DB::commit();

        return response()->json(['status' => $reservation,'message' => "تم تأكيد الحجز"]);
    }


}
