@extends('layouts.app')

@section('title','Expert Page')

@section('content')

<div class="container my-4">

<div class="border border-light p-3 mb-4">

  <div class="d-flex align-items-center justify-content-center" style="height: 350px">
    <div class="p-2 bd-highlight col-example">
    <img  class="rounded"  src="https://www.pavilionweb.com/wp-content/uploads/2017/03/man-300x300.png" alt="Card image cap" style=" height: 140px; ">
    </div>
    <div class="p-2 bd-highlight col-example">{{$expert->name}}</div>
    <div class="p-2 bd-highlight col-example">{{$expert->country}}</div>
    <div class="p-2 bd-highlight col-example">{{$expert->specialty}}</div>
    <div class="p-2 bd-highlight col-example" >Working Hours (in {{$expert->country}}): <span>{{$expert->start_time->format('Y-m-d h:i A')}} till {{$expert->close_time->format('Y-m-d h:i A')}} </span></div>
    <div class="p-2 bd-highlight col-example" >Working Hours (in {{ Request::get('city') }}): <span id="userTimeZone"></span></div>
<br>

@guest
<div class="p-2 bd-highlight col-example">you need to login before booking</div>
@endguest


@auth
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Book Now
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">BOOK FORM</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="POST" action="" >
  <div class="form-group">
  <div class="container">

   <div class="row">
   <input class="form-control" id="date" name="date" type="text"  autocomplete="off" style="width:40%" placeholder="Date">
   </div>

   <div class="row" style=" float: left; margin-top: 14px;">
   <lable >Timeslot (in minutes):</label><br>
   <label class="radio-inline"><input type="radio" value="15" name="timeslot" checked >15</label>
<label class="radio-inline"><input type="radio" value="30" name="timeslot" >30</label>
<label class="radio-inline"><input type="radio" value="45" name="timeslot">45</label>
<label class="radio-inline"><input type="radio" value="60" name="timeslot">60</label>
   </div>


   <div class="row" style="float: left;margin-top: 67px;margin-left: -160px !important;">

   <select id="workingHours" name="workingHours"  required>

   </select>
</div>

</div>


  </div>

</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">end</button>
        <button type="button" class="btn btn-primary" onclick="addReservation();">BOOK</button>
      </div>
    </div>
  </div>
</div>
@endauth
  </div>

</div>

</div>

<script>
    function showWorkingHours(){
     var start= moment("{{$expert->start_time}}").utcOffset({{ Request::get('offset') }}).format('YY/MM/DD hh:mm A');
     var end= moment("{{$expert->close_time}}").utcOffset({{ Request::get('offset') }}).format('YY/MM/DD hh:mm A');
     document.getElementById("userTimeZone").textContent=start + " till" + end;
    }
window.onload = showWorkingHours();

function changeWorkingHours(ts){
    $('#workingHours').empty();
  var start= moment("{{$expert->start_time}}").utcOffset({{ Request::get('offset') }}).format('hh:mm');
  var end= moment("{{$expert->close_time}}").utcOffset({{ Request::get('offset') }}).format('hh:mm');
  var rT=@json($expert->getReservedTimes());
times=generateTimeSlots(ts,rT,start,end);
times.forEach(function(entry){
    oFrom=parseTime(entry[0])  +" ==> "+ parseTime(entry[1]);
    oText=oFrom;
    oValue=entry;
  $("#workingHours").append(new Option(oText, oValue));


});





    }



function addReservation() {
  var date=$("#date").val();
  var timeslot=$('input[name="timeslot"]:checked').val();
  var time=$("#workingHours").val();
  $.ajax({  //get user geolocation info (ip, timezone ,city ...)
    beforeSend: function(){
      $('#loading').show();
  },
  complete: function(){
      $('#loading').hide();
  },
    url: '/api/book',
    type: 'post',
    dataType: 'JSON',
    data:{
        date:date,
        duration:timeslot,
        time:time,
        expert_id:{{$expert->id}},
        _token:"{{csrf_token()}}",
        user_id:"{{Auth::user()->id ?? 1}}"
        
            },
    success: function (data) {
alert(data.message);
    }
});
}
</script>

@endsection
