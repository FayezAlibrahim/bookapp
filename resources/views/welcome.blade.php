
@extends('layouts.app')

@section('title','Home Page')

@section('content')
<div class="row">
<div class="col-sm-12">

<select class="form-select" id="timeZoneList">
    <option value="all">Select Timezone:</option>
@foreach($timezones as $key => $value)
<option value="{{$key}}" label="{{$value}}">({{$key}})  {{$value}}</option>
@endforeach
</select>
</div>
</div>


<div class="row" style="margin-top:20px">
<div class="col-sm-12">

@each('layouts.experts', $experts, 'expert', 'record.no-items')

</div>
</div>
@endsection
 