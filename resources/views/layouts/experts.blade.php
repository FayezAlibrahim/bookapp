<div class="col-sm-4">
<div class="card" style="width: 18rem;">
  <img class="card-img-top" src="https://www.pavilionweb.com/wp-content/uploads/2017/03/man-300x300.png" alt="Card image cap" style=" height: 140px; ">
  <div class="card-body">
    <h5 class="card-title">{{$expert->name}}</h5>
    <p class="card-text">{{$expert->specialty}}.</p>
    <a href="#" class="btn btn-primary" onClick="showExpertPage({{$expert->id}})">More Detals</a>
  </div>
</div>
</div>
